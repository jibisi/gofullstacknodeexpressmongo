/* eslint no-unused-vars: ["error", { "args": "none" }] */
const Product = require('../models/product');

exports.createProduct = (req, res, next) => {
  const newProduct = new Product({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
    inStock: req.body.inStock
  });

  newProduct
    .save()
    .then((product) => {
      res.status(201).json({
        product
      });
    })
    .catch((error) => {
      res.status(400).json({
        error
      });
    });
};

exports.getProduct = (req, res, next) => {
  Product.findOne({
    _id: req.params.id
  })
    .then((product) => {
      res.status(200).json({
        product
      });
    })
    .catch((error) => {
      res.status(404).json({
        error
      });
    });
};

exports.modifyProduct = (req, res, next) => {
  const product = new Product({
    _id: req.params.id,
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
    inStock: req.body.inStock
  });

  Product.updateOne({ _id: req.params.id }, product)
    .then(() => {
      res.status(201).json({
        message: 'Modified!'
      });
    })
    .catch((error) => {
      req.status(400).json({
        error
      });
    });
};

exports.deleteProduct = (req, res, next) => {
  Product.deleteOne({ _id: req.params.id })
    .then(() => {
      res.status(200).json({
        message: 'Deleted!'
      });
    })
    .catch((error) => {
      res.status(404).json({
        error
      });
    });
};

exports.getAllProducts = (req, res, next) => {
  Product.find()
    .then((products) => {
      res.status(200).json({
        products
      });
    })
    .catch((error) => {
      res.status(404).json({
        error
      });
    });
};
