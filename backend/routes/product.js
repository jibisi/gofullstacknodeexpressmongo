/* eslint no-unused-vars: ["error", { "args": "none" }] */
const express = require('express');

const router = express.Router();

const productCtrl = require('../controllers/product');

// GET all products
router.get('/', productCtrl.getAllProducts);
// POST a product
router.post('/', productCtrl.createProduct);
// GET a product
router.get('/:id', productCtrl.getProduct);
// Update a Product
router.put('/:id', productCtrl.modifyProduct);
// Delete a Product
router.delete('/:id', productCtrl.deleteProduct);

module.exports = router;
