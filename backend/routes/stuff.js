/* eslint no-unused-vars: ["error", { "args": "none" }] */
const express = require('express');

const router = express.Router();
const auth = require('../middleware/auth');
const multer = require('../middleware/multer-config');
const stuffCtrl = require('../controllers/stuff');

// GET all things
router.get('/', auth, stuffCtrl.getAllStuff);
// POST a thing
router.post('/', auth, multer, stuffCtrl.createThing);
// GET a thing
router.get('/:id', auth, stuffCtrl.getOneThing);
// UPDATE a thing
router.put('/:id', auth, multer, stuffCtrl.modifyThing);
// DELETE a thing
router.delete('/:id', auth, stuffCtrl.deleteThing);

module.exports = router;
