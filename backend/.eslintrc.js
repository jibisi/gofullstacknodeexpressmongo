module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  extends: [
    'airbnb-base', 'prettier',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [ 
    'prettier'
  ],
  rules: {
    "comma-dangle": ["error", "never"],
    "object-curly-spacing": ["error", "always"],
    "prettier/prettier": ["error", {
      "endOfLine": "auto",
      "printWidth": 100,
      "singleQuote": true,
      "tabWidth": 2,
      "trailingComma": "none"
    }]
  },
};
